# iOS derivative service client

### Client for iOS allows you to solve derivatives of a functions and represent 2D plot.
The server side of the solver has been written on Scala language
Note, that 2D plot will be obtained only if result function has one argument, such as y = u(x)
![](math_solver_screenshot.png)
